package com.ytq.pratice.springbootmybatisplus.web;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.github.pagehelper.ISelect;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ytq.pratice.springbootmybatisplus.entity.Test;
import com.ytq.pratice.springbootmybatisplus.entity.User;
import com.ytq.pratice.springbootmybatisplus.service.TestService;
import com.ytq.pratice.springbootmybatisplus.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author change
 * @since 2018-09-03
 */
@RestController
@RequestMapping("/test")
public class TestController {

    @Autowired
    private TestService testService;

    @GetMapping("/hello")
    public String sayHello(){
        try {

            PageInfo<Test> pageInfo = PageHelper.startPage(1, 10).doSelectPageInfo(() ->
                    testService.selectList(new EntityWrapper<>(new Test()))
            );




            List<Test> list = pageInfo.getList();
            for (Test test : list) {
                System.out.println(test.toString());
            }
            if(pageInfo != null){
                return pageInfo.toString();
            }

        }catch (Exception e){
            e.printStackTrace();
            return e.getMessage();
        }
        return "没有查到呗";

    }

}

