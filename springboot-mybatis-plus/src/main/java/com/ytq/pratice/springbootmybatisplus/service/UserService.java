package com.ytq.pratice.springbootmybatisplus.service;

import com.baomidou.mybatisplus.service.IService;
import com.ytq.pratice.springbootmybatisplus.entity.User;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author change
 * @since 2018-08-31
 */
public interface UserService extends IService<User> {

}
