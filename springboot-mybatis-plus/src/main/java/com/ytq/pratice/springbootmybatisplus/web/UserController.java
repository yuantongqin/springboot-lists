package com.ytq.pratice.springbootmybatisplus.web;


import com.baomidou.mybatisplus.service.IService;
import com.ytq.pratice.springbootmybatisplus.entity.User;
import com.ytq.pratice.springbootmybatisplus.service.UserService;
import com.ytq.pratice.springbootmybatisplus.service.impl.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author change
 * @since 2018-08-31
 */
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("/hello")
    public String sayHello(@RequestParam("userId")Long userId){
        try {


            if(userService!= null){
                User user = userService.selectById(userId);
                return user.toString();
            }

        }catch (Exception e){
            e.printStackTrace();
            return e.getMessage();
        }
       return "没有查到呗";

    }

}

