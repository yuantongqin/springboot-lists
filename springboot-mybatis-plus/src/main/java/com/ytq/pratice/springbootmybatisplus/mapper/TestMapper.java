package com.ytq.pratice.springbootmybatisplus.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.ytq.pratice.springbootmybatisplus.entity.Test;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author change
 * @since 2018-09-03
 */
public interface TestMapper extends BaseMapper<Test> {

}
