package com.ytq.pratice.springbootmybatisplus.entity;

/**
 * User: hyman
 * Date: 2017/11/2 0002
 * Time: 21:43
 * Email: qyuhy@qq.com
 * To change this template use File | Settings | File Templates.
 */

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableLogic;

import java.io.Serializable;

/**
 * 实体父类
 */
public class SuperEntity<T extends Model> extends Model<T> {
    /**
     * 主键ID , 这里故意演示注解可以无
     */
    @TableId("id")
    protected Long id;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

//    /**
//     * 逻辑删除
//     */
//    @TableLogic
//    @TableField(value = "is_deleted")
//    private Integer isDeleted;
//
//    public Integer getIsDeleted() {
//        return isDeleted;
//    }
//
//    public void setIsDeleted(Integer isDeleted) {
//        this.isDeleted = isDeleted;
//    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }
}
