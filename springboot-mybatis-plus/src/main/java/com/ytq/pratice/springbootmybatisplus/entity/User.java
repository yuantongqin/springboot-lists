package com.ytq.pratice.springbootmybatisplus.entity;

import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author change
 * @since 2018-08-31
 */
@TableName("user")
public class User extends SuperEntity<User> {

    private static final long serialVersionUID = 1L;

    /**
     * 用户名（登录）
     */
    private String username;
    /**
     * 密码（登录）
     */
    private String password;
    /**
     * 真实姓名
     */
    private String realname;
    /**
     * 头像
     */
    private String avatar;
    /**
     * 手机号
     */
    private String mobile;
    private String sex;
    /**
     * 状态（1正常，2冻结）
     */
    private Integer status;
    /**
     * 创建时间
     */
    private Date createTime;



    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRealname() {
        return realname;
    }

    public void setRealname(String realname) {
        this.realname = realname;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    public String toString() {
        return "User{" +
        ", id=" + id +
        ", username=" + username +
        ", password=" + password +
        ", realname=" + realname +
        ", avatar=" + avatar +
        ", mobile=" + mobile +
        ", sex=" + sex +
        ", status=" + status +
        ", createTime=" + createTime +
        "}";
    }
}
