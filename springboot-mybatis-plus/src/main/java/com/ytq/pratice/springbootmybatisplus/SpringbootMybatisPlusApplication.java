package com.ytq.pratice.springbootmybatisplus;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@EnableTransactionManagement
@SpringBootApplication
//@ComponentScan(basePackages = {
//        "com.ytq.pratice.springbootmybatisplus.config",
//        "com.ytq.pratice.springbootmybatisplus.web",
//        "com.ytq.pratice.springbootmybatisplus.service",
//       })
public class SpringbootMybatisPlusApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootMybatisPlusApplication.class, args);
    }
}
