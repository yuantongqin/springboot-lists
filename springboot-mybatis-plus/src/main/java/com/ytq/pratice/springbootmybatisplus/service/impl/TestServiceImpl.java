package com.ytq.pratice.springbootmybatisplus.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.ytq.pratice.springbootmybatisplus.entity.Test;
import com.ytq.pratice.springbootmybatisplus.mapper.TestMapper;
import com.ytq.pratice.springbootmybatisplus.service.TestService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author change
 * @since 2018-09-03
 */
@Service
public class TestServiceImpl extends ServiceImpl<TestMapper, Test> implements TestService {

}
