package com.ytq.pratice.springbootmybatisplus.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.ytq.pratice.springbootmybatisplus.entity.User;
import com.ytq.pratice.springbootmybatisplus.mapper.UserMapper;
import com.ytq.pratice.springbootmybatisplus.service.UserService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author change
 * @since 2018-08-31
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

}
