package com.ytq.pratice.springbootmybatisplus.config;

import org.mybatis.spring.annotation.MapperScan;
import org.mybatis.spring.mapper.MapperScannerConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author yuantongqin
 * @date 2018/8/31 下午3:19
 */
@Configuration
//这个注解，作用相当于下面的@Bean MapperScannerConfigurer，2者配置1份即可
@MapperScan(basePackages = {"com.ytq.pratice.springbootmybatisplus.mapper*"})
public class MybatisPlusConfig {

//    @Bean
//    public MapperScannerConfigurer mapperScannerConfigurer() {
//        MapperScannerConfigurer scannerConfigurer = new MapperScannerConfigurer();
//        scannerConfigurer.setBasePackage("com.ytq.pratice.springbootmybatisplus.mapper*");
//        return scannerConfigurer;
//    }


}
