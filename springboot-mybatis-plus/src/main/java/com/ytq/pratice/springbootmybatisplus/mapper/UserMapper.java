package com.ytq.pratice.springbootmybatisplus.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.ytq.pratice.springbootmybatisplus.entity.User;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author change
 * @since 2018-08-31
 */
public interface UserMapper extends BaseMapper<User> {

}
