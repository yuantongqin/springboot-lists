package com.ytq.pratice.springbootmybatisplus.service;

import com.baomidou.mybatisplus.service.IService;
import com.ytq.pratice.springbootmybatisplus.entity.Test;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author change
 * @since 2018-09-03
 */
public interface TestService extends IService<Test> {

}
