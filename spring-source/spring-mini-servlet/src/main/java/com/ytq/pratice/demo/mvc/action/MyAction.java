package com.ytq.pratice.demo.mvc.action;

import com.ytq.pratice.demo.service.IDemoService;
import com.ytq.pratice.servlet.spring.annotation.Autowired;
import com.ytq.pratice.servlet.spring.annotation.Controller;
import com.ytq.pratice.servlet.spring.annotation.RequestMapping;

@Controller
public class MyAction {

    @Autowired
    private IDemoService demoService;

    @RequestMapping("/index.html")
    public void index(){

    }

}
