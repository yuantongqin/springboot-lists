package com.ytq.pratice.demo.service.impl;

import com.ytq.pratice.demo.service.IDemoService;
import com.ytq.pratice.servlet.spring.annotation.Service;

@Service("demoService")
public class DemoService implements IDemoService {

    @Override
    public String get(String name) {
        return "哈喽："+name;
    }
}
