package com.ytq.pratice.servlet.spring.annotation;

import java.lang.annotation.*;

/**
 * @author yuantongqin
 * @date 2018/9/25 下午5:35
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Controller {

    String name() default "";
}
