package com.ytq.pratice.demo.mvc.action;

import com.ytq.pratice.demo.service.IDemoService;
import com.ytq.pratice.servlet.spring.annotation.Autowired;
import com.ytq.pratice.servlet.spring.annotation.Controller;
import com.ytq.pratice.servlet.spring.annotation.RequestMapping;

@Controller(name = "demoAction")
@RequestMapping("/demo")
public class DemoAction {

    @Autowired
    private IDemoService demoService;

    @RequestMapping("/hello")
    public String sayHello(String name){
        String value = demoService.get(name);
        System.out.println(value);
        return value;
    }
}
