package com.ytq.pratice.strategy;

/**
 * @author yuantongqin
 * @date 2018/9/14 下午2:41
 */
public class AliPay implements PayChannel {

    @Override
    public PayState pay(String orderId, float amount) {

        System.out.println("使用支付支付");

        return new PayState(200,"西瓜20元","支付宝支付成功");
    }
}
