package com.ytq.pratice.strategy;

/**
 * @author yuantongqin
 * @date 2018/9/14 下午2:44
 */
public class PayTest {

    public static void main(String[] args) {

        

        Order order = new Order("201809140245",200);

        //这里采用支付宝支付，每次使用的时候需要用户自己去new 对象，而不是自己选择
        // 支付方法方式
//        order.pay(new AliPay());

        //这种方式不支持开闭原则，payChannel 定义了原有的pay()支付方法，
        // 后期添加渠道需要修改原来payChannel
//        order.pay(PayChannel.JD_PAY);

        //对payChannel进行扩展，或者把支付渠道放入枚举类中
//        order.pay(NewPayChannel.JD_PAY);

//        第三种方式既能解耦，又能更好的扩展
        order.pay(PayWay.WE_CHAT_PAY);

    }
}
