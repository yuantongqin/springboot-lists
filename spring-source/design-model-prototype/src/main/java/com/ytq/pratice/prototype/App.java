package com.ytq.pratice.prototype;

import com.ytq.pratice.prototype.QiTianDaShen;

import javax.sound.midi.Soundbank;

/**
 * Hello world!
 */
public class App {
    public static void main(String[] args) {

        QiTianDaShen daShen = new QiTianDaShen();
        try {
            daShen.name="斗战胜佛";
            QiTianDaShen clone = (QiTianDaShen) daShen.clone();

            System.out.println(daShen.jingubang);
            System.out.println(clone.jingubang);

            System.out.println(daShen.jingubang == clone.jingubang);

        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }

    }
}
