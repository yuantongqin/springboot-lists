package com.ytq.pratice.prototype;

import java.io.*;

/**
 * @author yuantongqin
 * @date 2018/9/10 上午11:14
 */
public class QiTianDaShen implements Cloneable,Serializable {

    public String name;
    public int age;
    public Jingubang jingubang = null;

    public QiTianDaShen() {
        this.name = "齐天大圣";
        this.age = 10000;
        jingubang = new Jingubang();
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return deepClone();
    }

    private Object deepClone(){
        ByteArrayOutputStream baos = null;
        try {
            baos = new ByteArrayOutputStream();
            ObjectOutputStream oos = new ObjectOutputStream(baos);
            oos.writeObject(this);

            ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
            ObjectInputStream ois = new ObjectInputStream(bais);
            Object o = ois.readObject();
            return o;

        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }finally {
            if(baos != null){
                try {
                    baos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }



}
