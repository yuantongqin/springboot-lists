package com.ytq.pratice.strategy;

/**
 * @author yuantongqin
 * @date 2018/9/14 下午2:42
 */
public class WeChatPay implements PayChannel {
    @Override
    public PayState pay(String orderId, float amount) {
        System.out.println("使用微信支付");
        return new PayState(200,"微信支付成功");
    }
}
