package com.ytq.pratice.strategy;

/**
 * @author yuantongqin
 * @date 2018/9/14 下午2:37
 *
 * 在商场下订单
 *
 */
public class Order {
    private String orderId;//订单编号
    private float amount;//订单金额

    public Order(String orderId, float amount) {
        this.orderId = orderId;
        this.amount = amount;
    }

    public PayState pay(PayWay channel){
        return channel.getPayChannel().pay(this.orderId,amount);
    }
//    public PayState pay(PayChannel channel){
//        return channel.pay(this.orderId,amount);
//    }


}