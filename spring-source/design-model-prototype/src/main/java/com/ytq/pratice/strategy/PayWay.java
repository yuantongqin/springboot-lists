package com.ytq.pratice.strategy;

/**
 * @author yuantongqin
 * @date 2018/9/14 下午2:54
 */
public enum PayWay {

    ALI_PAY(new AliPay()),
    WE_CHAT_PAY(new WeChatPay()) ,
    JD_PAY(new JDPay()) ;

    private PayChannel payChannel;

    PayWay(PayChannel payChannel) {
        this.payChannel = payChannel;
    }

    public PayChannel getPayChannel() {
        return payChannel;
    }
}
