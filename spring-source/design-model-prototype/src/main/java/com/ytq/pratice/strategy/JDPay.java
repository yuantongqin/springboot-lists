package com.ytq.pratice.strategy;

/**
 * @author yuantongqin
 * @date 2018/9/14 下午2:42
 */
public class JDPay implements PayChannel {
    @Override
    public PayState pay(String orderId, float amount) {
        System.out.println("使用京东支付");
        return new PayState(200,"京东支付成功");
    }
}
