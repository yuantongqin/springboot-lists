package com.ytq.pratice.strategy;

/**
 * @author yuantongqin
 * @date 2018/9/14 下午2:38
 * 支付状态
 */
public class PayState {

    private int code;//状态
    private String detail;//支付详情
    private String msg;//支付信息

    public PayState(int code, String detail, String msg) {
        this.code = code;
        this.detail = detail;
        this.msg = msg;
    }

    public PayState(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}
