package com.ytq.pratice.strategy;

/**
 * @author yuantongqin
 * @date 2018/9/14 下午2:38
 * 支付渠道
 */
public interface PayChannel {

    PayState pay(String orderId,float amount);

    PayChannel ALI_PAY = new AliPay();
    PayChannel WE_CHAT_PAY = new WeChatPay();
    PayChannel JD_PAY = new JDPay();


}
