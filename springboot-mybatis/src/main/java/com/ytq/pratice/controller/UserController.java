package com.ytq.pratice.controller;

import com.ytq.pratice.entity.User;
import com.ytq.pratice.service.TestService2;
import com.ytq.pratice.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author yuantongqin
 * @date 2018/8/30 下午6:58
 */
@RestController
public class UserController {

    Logger logger = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private UserService userService;

    @Autowired
    private TestService2 testService2;

    @GetMapping("/hello")
    public String sayHello(String name){
        logger.info("===输入的名字=="+name);
        userService.insertUser(null);
        return "你好"+name;
    }

    @GetMapping("/user")
    public User getUser(@RequestParam("phone")String phone){
        User user = null;
        try {
            user = userService.selectUserById(1L);
            logger.info(user.toString());
            logger.warn("==请求参数="+user.toString());
            logger.error(user.toString()+"==错误");
            return user;
        }catch (Exception e){
            e.printStackTrace();
        }

        return null;
    }

}
