package com.ytq.pratice.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author yuantongqin
 * 2019/6/14
 */
public class Orders implements Serializable {

    private Integer id;
    private Integer userId;
    private String number;
    private LocalDateTime createtime;
    private String note;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public LocalDateTime getCreatetime() {
        return createtime;
    }

    public void setCreatetime(LocalDateTime createtime) {
        this.createtime = createtime;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}
