package com.ytq.pratice.service;

import java.util.Date;

import com.ytq.pratice.dao.UserMapper;
import com.ytq.pratice.entity.User;
import com.ytq.pratice.exceptioin.TestException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author yuantongqin
 * 2019/5/6
 */
@Service
public class TestService2 {

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private TestService1 testService1;

    public void sa(){
        System.out.println("这里不启动事务");
        //这里插入数据库
        User user = new User();
        user.setPhone("123450678");
        user.setUsername("张三3");
        user.setPassword("123456");
        user.setSex((byte) 0);
        user.setCreateTime(new Date());
        user.setIsDeleted((byte) 0);
        user.setCreateTime(new Date());
        user.setResourceType((byte) 0);
        int insert = userMapper.insert(user);
        System.out.println(insert);
//        sb();
        testService1.insertUserStartTransaction();
    }

    @Transactional(rollbackFor = TestException.class)
    public void sb(){
        System.out.println("这里启动事务sb");
        sa();
        //这里抛异常
        throw new TestException("出错了");
    }

}
