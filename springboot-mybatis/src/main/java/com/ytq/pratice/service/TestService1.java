package com.ytq.pratice.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author yuantongqin
 * 2019/5/6
 */
@Service
public class TestService1 {


    @Transactional(isolation = Isolation.REPEATABLE_READ)
    public void insertUserStartTransaction(){
        System.out.println("这里有什么东西吗");
    }



}
