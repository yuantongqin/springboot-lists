package com.ytq.pratice.service;

import com.ytq.pratice.api.UserApi;
import com.ytq.pratice.dao.ItemsMapper;
import com.ytq.pratice.dao.OrdersMapper;
import com.ytq.pratice.dao.UserMapper;
import com.ytq.pratice.entity.Items;
import com.ytq.pratice.entity.Orders;
import com.ytq.pratice.entity.User;
import com.ytq.pratice.exceptioin.TestException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionTemplate;

/**
 * @author yuantongqin
 * @date 2018/8/30 下午6:59
 */
@Service
public class UserService implements UserApi{


    @Autowired
    private UserMapper userMapper;
    @Autowired
    private ItemsMapper itemsMapper;
    @Autowired
    private OrdersMapper ordersMapper;
    @Autowired
    UserService userService;

    @Autowired
    private TransactionTemplate transactionTemplate;
    @Override
    public User selectUserById(Long userId) {
        User user = userMapper.selectByPrimaryKey(userId);
        return user;
    }


    @Transactional
    @Override
    public int insertUser(User user) {
        Orders orders = new Orders();
        orders.setUserId(2);
        orders.setNumber("李四");
        ordersMapper.insert(orders);


        try {
            transactionTemplate.execute(t->{
                Items items = new Items();
                items.setName("张三");
                items.setPrice(20.3F);
                items.setDetail("哈哈哈哈");
                itemsMapper.insert(items);
               throw new TestException("fdsafdsa");
            });
        }catch (Exception e){
            e.printStackTrace();
            System.out.println("失败");
        }

        return 1;
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void insertItems(){

    }



    @Override
    public int insertByNotExist(User user) {
        return userMapper.insertByNotExist(user);
    }


}
