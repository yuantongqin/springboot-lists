package com.ytq.pratice.api;

import com.ytq.pratice.entity.User;

/**
 * @author yuantongqin
 * @date 2018/8/30 下午7:06
 */
public interface UserApi {

    /**
     * 根据id获取用户信息
     */
    User selectUserById(Long userId);

    int insertUser(User user);

    int insertByNotExist(User user);


}
