package com.ytq.pratice.dao;


import com.ytq.pratice.entity.Items;
import com.ytq.pratice.entity.Orders;
import com.ytq.pratice.entity.User;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface OrdersMapper {

    int insert(Orders orders);

}