package com.ytq.pratice.exceptioin;

/**
 * @author yuantongqin
 * 2019/5/6
 */
public class TestException extends RuntimeException {

    public TestException(String message){
        super(message);
        System.out.println("启动了异常");
    }

}
