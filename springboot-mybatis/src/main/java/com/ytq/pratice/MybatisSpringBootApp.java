package com.ytq.pratice;

import org.mybatis.spring.annotation.MapperScan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Hello world!
 */
@SpringBootApplication
@MapperScan(basePackages={"com.ytq.pratice"})
public class MybatisSpringBootApp {
    private  static Logger LOGGER = LoggerFactory.getLogger(MybatisSpringBootApp.class);

    public static void main(String[] args) {

//        LOGGER.info("开始启动 ");
        SpringApplication.run(MybatisSpringBootApp.class,args);
//        LOGGER.info("启动完成");

    }
}
