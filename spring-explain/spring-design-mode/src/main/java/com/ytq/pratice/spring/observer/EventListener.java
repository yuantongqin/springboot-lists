package com.ytq.pratice.spring.observer;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

/**
 * @author yuantongqin
 * @date 2018/9/17 下午2:35
 */
public class EventListener {


    private Map<Enum,Event> listener = new HashMap<>();
    public void addListener(Enum e, Object target, Method list){
        listener.put(e,new Event(this,target,list));
    }

    void trigger(TypeEnum typeEnum){
        Event event = listener.get(typeEnum);
        if(event != null){
            try {
                event.getCallback().invoke(event.getTarget(),null);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
        }
    }



}
