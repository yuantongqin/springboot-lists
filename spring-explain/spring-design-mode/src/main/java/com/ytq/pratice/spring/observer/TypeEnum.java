package com.ytq.pratice.spring.observer;

/**
 * @author yuantongqin
 * @date 2018/9/17 下午2:42
 */
public enum TypeEnum {
    ON_CLICK,
    OFF_CLICK;

}
