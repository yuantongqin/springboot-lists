package com.ytq.pratice.spring.observer;

/**
 * @author yuantongqin
 * @date 2018/9/17 下午2:34
 * 观察者
 */
public class Light {


    public void OnLight(){
        System.out.println("开灯");
    }

    public void OffLight(){
        System.out.println("关灯");
    }

}
