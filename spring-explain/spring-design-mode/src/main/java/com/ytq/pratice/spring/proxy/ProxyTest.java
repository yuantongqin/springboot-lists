package com.ytq.pratice.spring.proxy;

/**
 * @author yuantongqin
 * @date 2018/9/17 下午4:22
 */
public class ProxyTest {

    public static void main(String[] args) {

        ProxyService proxyService = new ProxyService(new ServiceImpl());
        IService iService = proxyService.getProxy();
        System.out.println(iService.getName("哈哈哈"));
    }
}
