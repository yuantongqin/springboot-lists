package com.ytq.pratice.spring.observer;

import java.lang.reflect.Method;
import java.util.List;

/**
 * @author yuantongqin
 * @date 2018/9/17 下午2:36
 */
public class ObserverTest {

    public static void main(String[] args) {
        try {
            Light light = new Light();

            Method method = light.getClass().getMethod("OnLight", new Class[]{});
            ButtonOnOff button = new ButtonOnOff();
            button.addListener(TypeEnum.ON_CLICK, light,method);
            button.On();

        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }

    }
}
