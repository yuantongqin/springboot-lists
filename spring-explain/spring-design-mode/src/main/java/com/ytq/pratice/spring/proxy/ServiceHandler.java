package com.ytq.pratice.spring.proxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * @author yuantongqin
 * @date 2018/9/17 下午4:24
 */
public class ServiceHandler implements InvocationHandler {

    private IService service;

    public ServiceHandler(IService service) {
        this.service = service;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {

       if(1==1){
          return method.invoke(service,args);
       }

        return method.invoke(proxy,args);
    }
}
