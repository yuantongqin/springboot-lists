package com.ytq.pratice.spring.template.dao;

import com.ytq.pratice.spring.template.JdbcTemplate;
import com.ytq.pratice.spring.template.entity.User;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * @author yuantongqin
 * @date 2018/9/16 上午11:17
 */
public class UserDao   {

    private JdbcTemplate jdbcTemplate = new JdbcTemplate(null);

    /**
     * jdbcTemplate中的主流程，我们不需要去变动
     * 我们只需要自定义processResult方法
     * @return
     */

    public List<?> query(){
        String sql = "select * from user";
        return jdbcTemplate.getQuery(sql, new RowMapper<Object>() {
            @Override
            public Object rowResult(ResultSet resultSet) {
                User user = new User();
                try {
                    user.setId(resultSet.getInt("id"));
                    user.setUsername(resultSet.getString("username"));
                    user.setPassword(resultSet.getString("password"));
                    user.setRealname(resultSet.getString("realname"));
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                return user;
            }
        });
    }


}
