package com.ytq.pratice.spring.factory.abstr;

import com.ytq.pratice.spring.factory.Milk;

/**
 * @author yuantongqin
 * @date 2018/9/7 下午3:30
 */
public abstract class AbstractFactory {

    /**
     * 接口与抽象类的不同，抽象类中不仅可以有抽象方法，还可以做一些公用的实现
     */

    public abstract Milk getMengNiuMilk();

    public abstract Milk getTeLunSuMilk();

    public abstract Milk getYiLiMilk();

}
