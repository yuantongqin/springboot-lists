package com.ytq.pratice.spring.factory.func;

import com.ytq.pratice.spring.factory.Milk;
import com.ytq.pratice.spring.factory.TeLunSuMilk;

/**
 * @author yuantongqin
 * @date 2018/9/7 下午3:24
 */
public class TeLunsuFactory implements Factory{

    @Override
    public Milk getMilk() {
        return new TeLunSuMilk();
    }
}
