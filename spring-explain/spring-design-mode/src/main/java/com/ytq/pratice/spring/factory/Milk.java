package com.ytq.pratice.spring.factory;

/**
 * @author yuantongqin
 * @date 2018/9/7 下午2:38
 */
public interface Milk {

    /**
     * 牛奶的类型
     */
    String getName();
}
