package com.ytq.pratice.spring.observer;

import java.util.HashMap;
import java.util.Map;

/**
 * @author yuantongqin
 * @date 2018/9/17 下午2:30
 * 是开关
 */
public class ButtonOnOff extends EventListener{

    public void On(){
        System.out.println("当我点击打开事件是会有灯亮起来");
        trigger(TypeEnum.ON_CLICK);

    }

    public void off(){
        System.out.println("当我点击关闭按钮时会有灯关闭");
        trigger(TypeEnum.OFF_CLICK);
    }
}
