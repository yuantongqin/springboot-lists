package com.ytq.pratice.spring.template.dao;

import java.sql.ResultSet;

/**
 * @author yuantongqin
 * @date 2018/9/16 上午11:50
 */
public interface RowMapper<T> {

    T rowResult(ResultSet set);
}
