package com.ytq.pratice.spring.factory.abstr;

import com.ytq.pratice.spring.factory.Milk;

/**
 * @author yuantongqin
 * @date 2018/9/7 下午3:33
 */
public class AbstractFactoryTest {

    public static void main(String[] args) {
        AbstractFactory factory = new FactoryImpl();
        Milk milk = factory.getMengNiuMilk();
        factory.getTeLunSuMilk();
        System.out.println(milk.getName());

        /**
         * 抽象工厂模式，用户只管自己想要什么产品
         * 工厂只负责提供产品，产品的生成过程不需要告诉用户
         */

    }
}
