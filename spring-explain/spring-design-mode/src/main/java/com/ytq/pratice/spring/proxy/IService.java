package com.ytq.pratice.spring.proxy;

/**
 * @author yuantongqin
 * @date 2018/9/17 下午4:23
 */
public interface IService {

    String getName(String name);
}
