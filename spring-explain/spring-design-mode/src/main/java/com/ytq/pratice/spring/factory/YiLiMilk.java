package com.ytq.pratice.spring.factory;

/**
 * @author yuantongqin
 * @date 2018/9/7 下午3:12
 */
public class YiLiMilk implements Milk {
    @Override
    public String getName() {
        return "生成伊利牛奶";
    }
}
