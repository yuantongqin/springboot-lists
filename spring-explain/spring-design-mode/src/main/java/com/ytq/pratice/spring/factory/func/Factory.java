package com.ytq.pratice.spring.factory.func;

import com.ytq.pratice.spring.factory.Milk;

/**
 * @author yuantongqin
 * @date 2018/9/7 下午3:24
 */
public interface Factory {

    Milk getMilk();
}
