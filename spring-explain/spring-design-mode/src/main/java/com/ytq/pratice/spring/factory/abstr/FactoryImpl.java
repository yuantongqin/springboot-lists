package com.ytq.pratice.spring.factory.abstr;

import com.ytq.pratice.spring.factory.MengNiuMilk;
import com.ytq.pratice.spring.factory.Milk;
import com.ytq.pratice.spring.factory.YiLiMilk;
import com.ytq.pratice.spring.factory.func.TeLunsuFactory;

/**
 * @author yuantongqin
 * @date 2018/9/7 下午3:32
 */
public class FactoryImpl extends AbstractFactory {
    @Override
    public Milk getMengNiuMilk() {
        return new MengNiuMilk();
    }

    @Override
    public Milk getTeLunSuMilk() {
        return new TeLunsuFactory().getMilk();
    }

    @Override
    public Milk getYiLiMilk() {
        return new YiLiMilk();
    }
}
