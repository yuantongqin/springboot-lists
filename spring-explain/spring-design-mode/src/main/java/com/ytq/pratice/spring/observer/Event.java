package com.ytq.pratice.spring.observer;

import java.lang.reflect.Method;

/**
 * @author yuantongqin
 * @date 2018/9/17 下午2:35
 */
public class Event {

    private Object source;//事件源
    private Object target;//目标对象
    private Method callback;//回调方法

    public Event(Object source, Object target, Method callback) {
        this.source = source;
        this.target = target;
        this.callback = callback;
    }

    public Object getSource() {
        return source;
    }

    public void setSource(Object source) {
        this.source = source;
    }

    public Object getTarget() {
        return target;
    }

    public void setTarget(Object target) {
        this.target = target;
    }

    public Method getCallback() {
        return callback;
    }

    public void setCallback(Method callback) {
        this.callback = callback;
    }
}
