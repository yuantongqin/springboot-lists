package com.ytq.pratice.spring.factory.func;

import com.ytq.pratice.spring.factory.Milk;
import com.ytq.pratice.spring.factory.YiLiMilk;

/**
 * @author yuantongqin
 * @date 2018/9/7 下午3:24
 */
public class YiLiFactory implements Factory {

    @Override
    public Milk getMilk() {
        return new YiLiMilk();
    }
}
