package com.ytq.pratice.spring.factory.simple;

import com.ytq.pratice.spring.factory.MengNiuMilk;
import com.ytq.pratice.spring.factory.Milk;
import com.ytq.pratice.spring.factory.TeLunSuMilk;
import com.ytq.pratice.spring.factory.YiLiMilk;

/**
 * @author yuantongqin
 * @date 2018/9/7 下午3:14
 * 简单工厂模式，什么都生产
 */
public class SimpleFactroy {

    public Milk getMilk(String name){
        if("蒙牛".equals(name)){
            return new MengNiuMilk();
        } else if("特仑苏".equals(name)){
            return new TeLunSuMilk();
        } else if("伊利".equals(name)){
            return new YiLiMilk();
        }
        return null;
    }
}
