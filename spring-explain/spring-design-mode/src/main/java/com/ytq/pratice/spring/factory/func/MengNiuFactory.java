package com.ytq.pratice.spring.factory.func;

import com.ytq.pratice.spring.factory.MengNiuMilk;
import com.ytq.pratice.spring.factory.Milk;

/**
 * @author yuantongqin
 * @date 2018/9/7 下午3:23
 */
public class MengNiuFactory implements Factory {
    @Override
    public Milk getMilk() {
        return new MengNiuMilk();
    }
}
