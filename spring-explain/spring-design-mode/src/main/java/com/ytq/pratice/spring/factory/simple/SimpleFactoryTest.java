package com.ytq.pratice.spring.factory.simple;

import com.ytq.pratice.spring.factory.Milk;

/**
 * @author yuantongqin
 * @date 2018/9/7 下午3:15
 */
public class SimpleFactoryTest {

    public static void main(String[] args) {
        SimpleFactroy factroy=new SimpleFactroy();
        /*弊端需要用户自己填写需要用到的产品，而不是让用户去选择
        就像你跑到大型超市去买东西，超市空空如也，然后你跑个去问服务员，你这有蒙牛牛奶吗，
        服务员到仓库帮你拿到蒙牛牛奶，你问他有没有特仑苏，然后他又跑到仓库帮你拿来特仑苏
        这对用户来说不友好；
         */
        Milk 蒙牛 = factroy.getMilk("蒙牛");
        System.out.println(蒙牛);
    }
}
