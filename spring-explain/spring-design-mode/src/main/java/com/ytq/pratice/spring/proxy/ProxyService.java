package com.ytq.pratice.spring.proxy;

import java.lang.reflect.Proxy;

/**
 * @author yuantongqin
 * @date 2018/9/17 下午4:24
 */
public class ProxyService {

    private IService service;

    public ProxyService(IService service) {
        this.service = service;
    }

    public <T> T getProxy(){
       return (T) Proxy.newProxyInstance(service.getClass().getClassLoader(), service.getClass().getInterfaces(), new ServiceHandler(service));
    }


}
