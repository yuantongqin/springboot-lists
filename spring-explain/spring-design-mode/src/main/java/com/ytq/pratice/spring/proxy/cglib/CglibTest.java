package com.ytq.pratice.spring.proxy.cglib;

/**
 * @author yuantongqin
 * @date 2018/9/17 下午5:01
 */
public class CglibTest {

    public static void main(String[] args) {

       ZhangSan o
        = (ZhangSan) new CglibProxy().getInstance(ZhangSan.class);
       o.findLove();
        System.out.println(o);

    }
}
