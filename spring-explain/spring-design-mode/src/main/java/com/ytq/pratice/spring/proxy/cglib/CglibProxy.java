package com.ytq.pratice.spring.proxy.cglib;

import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

/**
 * @author yuantongqin
 * @date 2018/9/17 下午4:59
 */
public class CglibProxy implements MethodInterceptor {

    public Object getInstance(Class<?> clz){
        Enhancer enhancer = new Enhancer();
        //需要将谁设置为即将生成的父类
        enhancer.setSuperclass(clz);
        enhancer.setCallback(this);
        return enhancer.create();
    }

    @Override
    public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {

        System.out.println("通过cglib实现静态代理");

        return methodProxy.invokeSuper(o,objects);


    }
}
