package com.ytq.pratice.spring.factory.func;

import com.ytq.pratice.spring.factory.Milk;

/**
 * @author yuantongqin
 * @date 2018/9/7 下午3:25
 */
public class FactoryTest {

    public static void main(String[] args) {

        Factory factory=new MengNiuFactory();
        Milk milk = factory.getMilk();

        /**
         * 现在我们的工厂不是一个大杂烩了，我们的工厂只专注于做一件事
         * 但是对用户来说也是一个麻烦事，不能自己选择，也是需要服务器去拿，
         * 只是服务器根据不同的需求去分类仓库中拿，而不是一个大仓库拿了；
         */

        System.out.println(milk.getName());

    }
}
