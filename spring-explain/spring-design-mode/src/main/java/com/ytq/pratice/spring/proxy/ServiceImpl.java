package com.ytq.pratice.spring.proxy;

/**
 * @author yuantongqin
 * @date 2018/9/17 下午4:23
 */
public class ServiceImpl implements IService {


    @Override
    public String getName(String name) {
        return "你好："+name;
    }
}
