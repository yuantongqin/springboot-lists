package com.ytq.pratice.receiver;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * yuantongqin
 * 2019/2/25
 */
@Component
@RabbitListener(queues = "say")
public class DefaultReceiver {

    @RabbitHandler
    public void sayHello(String hello){
        System.out.println("say receiver 接受到了什么："+hello);
    }



}
