package com.ytq.pratice.receiver;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * yuantongqin
 * 2019/2/25
 */
@Component
@RabbitListener(queues = "hello")
public class HelloReceiver {

    @RabbitHandler
    public void sayHello(String hello){
        System.out.println("hello receiver 接受到了什么："+hello);
    }



}
