package com.ytq.pratice.receiver;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;

/**
 * yuantongqin
 * 2019/2/26
 */
public class Tut3Receiver {

    @Value("#{autoDeleteQueue1.name}")
    String name1;
    @Value("#{autoDeleteQueue2.name}")
   public String name2;

    @RabbitListener(queues = "#{autoDeleteQueue1.name}")
    public void receiver1(String in) throws InterruptedException {
        receiver(in,1);
    }

    @RabbitListener(queues = "#{autoDeleteQueue2.name}")
    public void receiver2(String in) throws InterruptedException {
        receiver(in,2);
    }

    public void receiver(String in,int receiver) throws InterruptedException{
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        System.out.println("instance " + receiver + " [x] Received '" + in + "'");
        doWork(in);
        stopWatch.stop();
        System.out.println("总共时间："+stopWatch.getTotalTimeSeconds());
    }

   private void doWork(String in) throws InterruptedException {
       for (char c : in.toCharArray()) {
           if(c == '.'){
               Thread.sleep(1000);
           }
       }
   }



}
