package com.ytq.pratice.config;

import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

/**
 * yuantongqin
 * 2019/2/22
 */
@Profile("tut1")
@Configuration
public class RabbitmqConfig {

    @Bean
    public Queue defaultQueue(){
        return new Queue("say");
    }

    @Bean Queue helloQueue(){
        return new Queue("hello");
    }

}
