package com.ytq.pratice.config;

import com.ytq.pratice.receiver.Tut3Receiver;
import com.ytq.pratice.sender.Tut3Sender;
import org.springframework.amqp.core.AnonymousQueue;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

/**
 * yuantongqin
 * 2019/2/26
 */
@Profile({"tut3","pub-sub","publish-subscribe"})
@Configuration
public class Tut3Config {

    @Bean
    public FanoutExchange fanout(){
        return new FanoutExchange("tut.fanout");
    }

    @Bean
    public ReceiverConfig receiverConfig(){
        return new ReceiverConfig();
    }

    @Profile("receiver")
    private static class ReceiverConfig{

        @Bean //自动删除队列
        public Queue autoDeleteQueue1(){
            //匿名的队列
            return new AnonymousQueue();
        }

        @Bean
        public Queue autoDeleteQueue2(){
            return new AnonymousQueue();
        }

        //将交换机与queue队列进行绑定
        @Bean
        public Binding binding1(FanoutExchange fanout,
                                Queue autoDeleteQueue1) {
            return BindingBuilder.bind(autoDeleteQueue1).to(fanout);
        }
        //这里是一个交换机绑定了两个队列queue
        @Bean
        public Binding binging2(FanoutExchange fanout,Queue autoDeleteQueue2){
            return BindingBuilder.bind(autoDeleteQueue2).to(fanout);
        }

        @Bean
        public Tut3Receiver receiver() {
            return new Tut3Receiver();
        }
    }

    @Profile("sender")
    @Bean
    public Tut3Sender sender(){
        return new Tut3Sender();
    }


}
