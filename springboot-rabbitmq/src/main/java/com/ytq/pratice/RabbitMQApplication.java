package com.ytq.pratice;

import java.util.concurrent.CountDownLatch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * Hello world!
 */
@SpringBootApplication
@EnableScheduling
public class RabbitMQApplication {

    public static void main(String[] args) {
        CountDownLatch latch = new CountDownLatch(1);
        SpringApplication.run(RabbitMQApplication.class,args);
        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
