package com.ytq.pratice.sender;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * yuantongqin
 * 2019/2/25
 */
@Component
public class TestSender {

    @Autowired
    private AmqpTemplate amqpTemplate;

    public String hello(String name){

        String key = "hello";
        String content = "内容发送者："+name;
        System.out.println("发送者："+content);
        amqpTemplate.convertAndSend(key,content);
        return ""+name;
    }

    public void sayHello(String say){
        String key = "say";
        String content = "第二个接受者say :" + say;
        System.out.println("say 发送者："+content);
        amqpTemplate.convertAndSend(key,content);
    }
}
