package com.ytq.pratice.sender;

import java.util.Collection;
import java.util.concurrent.atomic.AtomicInteger;

import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.Scheduled;

/**
 * yuantongqin
 * 2019/2/26
 */
@Profile("sender")
public class Tut3Sender {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    FanoutExchange fanout;

    AtomicInteger dots = new AtomicInteger(0);

    AtomicInteger count = new AtomicInteger(0);

    @Value("#{autoDeleteQueue1.name}")
    String name1;


    @Scheduled(fixedDelay = 1000,initialDelay = 500)
    public void send(){
        StringBuilder sb = new StringBuilder("hello");
        if(dots.incrementAndGet() == 3){
            dots.set(0);
        }
        for (int i = 0; i < dots.get(); i++) {
            sb.append(".");
        }
        sb.append(count.incrementAndGet());
        String content = sb.toString();
        rabbitTemplate.convertAndSend(fanout.getName(),content);
        System.out.println("发送者："+fanout.getName()+"==发送的内容："+content);

    }

    public void ss(){

    }

}
