package com.ytq.pratice.java.list;

/**
 * @author yuantongqin
 * @date 2018/10/10 上午11:01
 */
public class ListNode {

    public  String value;
    public ListNode next;

    public ListNode(String value) {
        this.value = value;
    }
    public ListNode(String value, ListNode next) {
        this.value = value;
        this.next = next;
    }

    @Override
    public String toString() {
        return "ListNode{" +
                "value='" + value + '\'' +
                ", next=" + next +
                '}';
    }
}
