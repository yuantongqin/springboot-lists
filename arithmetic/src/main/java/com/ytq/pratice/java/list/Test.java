package com.ytq.pratice.java.list;

/**
 * @author yuantongqin
 * @date 2018/10/10 下午2:28
 */
public class Test {

    /**
     * 反转
     */
    public static ListNode reverse(ListNode head) {
        ListNode pre = null;
        ListNode next = null;
        while (head != null) {
            next = head.next;
            head.next = pre;
            pre = head;
            head = next;
        }

        return pre;
    }

    /**
     * 取中间值:如果是偶数个取中间值的第一个
     */
    public static ListNode getCenter(ListNode head){
        if(head == null){
            return head;
        }
        ListNode fast = head;
        ListNode slow = head;
        while (head.next!= null&& head.next.next != null){
            slow = slow.next;
            fast = fast.next.next;
        }
        return slow;
    }


}
