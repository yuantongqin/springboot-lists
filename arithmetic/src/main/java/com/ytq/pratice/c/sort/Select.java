package com.ytq.pratice.c.sort;

/**
 * @author yuantongqin
 * @date 2018/10/10 下午3:00
 */
public class Select {


    public static void selectSort(){
        int[] s = {10,5,9,7};
        for (int i = 0;i<s.length;i++) {
            int a = s[i];
            for (int j=i;j<s.length;j++){
                int b = s[j];
                if(a>b){
                    s[i] = b;
                    s[j] = a;
                    a = b;
                }
            }
        }
        System.out.println(s.length);
        for (int i=0;i<s.length;i++) {
            System.out.print(s[i]+",");
        }

    }

    public static void main(String[] args) {
        selectSort();
    }
}
