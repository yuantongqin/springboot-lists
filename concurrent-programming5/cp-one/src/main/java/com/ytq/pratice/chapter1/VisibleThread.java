package com.ytq.pratice.chapter1;

import java.util.concurrent.TimeUnit;

/**
 * @author yuantongqin
 * @date 2018/9/4 下午2:20
 */
public class VisibleThread {

    private volatile static boolean stop = false;
    public static void main(String[] args) throws InterruptedException {
        Thread thread = new Thread(()->{
            int i= 0;
            while (!stop){
                i++;
            }
        });
        thread.start();
        TimeUnit.SECONDS.sleep(1);
        stop=true;
    }





}
