package com.ytq.pratice.interview.entity;

/**
 * @author yuantongqin
 * @date 2018/9/23 上午10:41
 */
public class User {

    private String name;

    public User(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                '}';
    }
}
