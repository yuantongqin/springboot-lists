package com.ytq.pratice.chapter1;

/**
 * @author yuantongqin
 * @date 2018/9/4 下午4:23
 */
public class AtomicThread {

    private static int count = 0;

    public static void inc(){
        try {
            Thread.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        count ++;

    }

    public static void main(String[] args) {
        for (int i=0;i<1000;i++){
            new Thread(()->{
                inc();
            }).start();
        }

        System.out.println("==值=="+count);

//        Thread
    }
}
