package com.ytq.pratice.chapter1;




import javax.script.ScriptException;
import java.util.concurrent.*;

/**
 * @author yuantongqin
 * @date 2018/9/3 下午1:41
 */
public class ProcedureCallable implements Callable<String> {


    public static void main(String[] args) {
        ExecutorService service = Executors.newCachedThreadPool();


        ProcedureCallable call = new ProcedureCallable();
        Future<String> future = service.submit(call);
        try {
            String s = future.get();//get获取的时候是阻塞状态
            System.out.println(s);
            service.shutdown();

        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

    }

    @Override
    public String call() throws Exception {
        return "String"+1;
    }
}
