package com.ytq.pratice;

import java.util.concurrent.TimeUnit;

/**
 * Hello world!
 */
public class TheadApp {


    static  volatile  int a = 10;

    public static void main(String[] args) {
        System.out.println("Hello World!");

        new Thread("timesleep"){
            @Override
            public void run() {
                while (true){
                    try {
                        TimeUnit.SECONDS.sleep(20);
                        System.out.println("睡上20秒");

                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }.start();

        new Thread("timewaiting"){
            @Override
            public void run() {
                while (true){
                    synchronized (TheadApp.class){
                        try {
                            TheadApp.class.wait();
                            System.out.println("直接阻塞不睡了");
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }

                }
            }
        }.start();

//       final Waiting waiting =  new Waiting();
        new Thread(new Waiting(),"blocking-1").start();

        new Thread(new Waiting(),"blocking-2").start();


    }

    public static class Waiting extends Thread{

        @Override
        public void run(){
            synchronized (Waiting.class){
//                while (true){
                    try {
                        System.out.println("执行executor方法");
                        TimeUnit.SECONDS.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
//                }


            }
        }
    }







}
