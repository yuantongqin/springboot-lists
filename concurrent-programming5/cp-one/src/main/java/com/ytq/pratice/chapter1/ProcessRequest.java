package com.ytq.pratice.chapter1;

/**
 * @author yuantongqin
 * @date 2018/9/3 下午1:30
 */
public interface ProcessRequest {

    void realRequest(Request request);
}
