package com.ytq.pratice.chapter1;

import java.util.concurrent.LinkedBlockingQueue;

/**
 * @author yuantongqin
 * @date 2018/9/3 下午1:34
 */
public class SaveProcessRequest extends Thread implements ProcessRequest {

    LinkedBlockingQueue queue = new LinkedBlockingQueue();

    @Override
    public void run() {
        while (true){
            try {
                Request request = (Request) queue.take();
                System.out.println("save data ="+request);


            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    }

    @Override
    public void realRequest(Request request) {
        queue.add(request);
    }
}
