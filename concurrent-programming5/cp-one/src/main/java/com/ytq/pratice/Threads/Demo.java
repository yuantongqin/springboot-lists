package com.ytq.pratice.Threads;

import com.ytq.pratice.interview.entity.User;

public class Demo {

    User user = new User("");

    public static void main(String[] args) {

    }

    class  Thread1 extends Thread{
        User user;
        public Thread1(User user){
            this.user
                     = user;
        }

        @Override
        public void run() {

        }
    }

    class Thread2 extends Thread{
        User user;
        public Thread2(User user){
            this.user = user;
        }

        @Override
        public void run() {
            synchronized (Thread2.class){
                try {
                    user.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }
        }
    }

}
