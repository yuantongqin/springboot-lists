package com.ytq.pratice.chapter1;

import java.util.concurrent.LinkedBlockingQueue;

/**
 * @author yuantongqin
 * @date 2018/9/3 下午1:33
 */
public class PrintProcessRequest extends Thread implements ProcessRequest  {

    LinkedBlockingQueue queue = new LinkedBlockingQueue();
    ProcessRequest processRequest;

    public PrintProcessRequest(ProcessRequest request){
        this.processRequest = request;
    }

    @Override
    public void run() {

        while (true){
            try {
                Request request = (Request) queue.take();
                System.out.println("print data "+request);
                processRequest.realRequest(request);

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void realRequest(Request request) {
        System.out.println("==这里是添加到队列==");
        queue.add(request);
    }


}
