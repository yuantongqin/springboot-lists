package com.ytq.pratice.chapter1;

/**
 * @author yuantongqin
 * @date 2018/9/3 下午1:56
 */
public class App {

    PrintProcessRequest print;
    public App() {
        SaveProcessRequest save = new SaveProcessRequest();
        save.start();
        print = new PrintProcessRequest(save);
        print.start();
    }

    public static void main(String[] args) {
        Request request = new Request();
        request.setName("张三");
        new App().doTest(request);

    }

    public void doTest( Request request ){
        print.realRequest(request);
    }
}
