package com.ytq.pratice.chapter2;

/**
 * @author yuantongqin
 * @date 2018/9/5 下午2:00
 */
public class BarrierDemo {

    private static int a = 0, b = 0;
    private static int x = 0, y = 0;
    public static void main(String[] args) {
        Thread thread1 = new Thread(() -> {
            a = 1;
            x = b;
        });
        Thread thread2 = new Thread(() -> {
            b = 1;
            y = a;
        });
        thread1.start();
        thread2.start();
        try {
            thread1.join();
            thread2.join();
            System.out.println(x+"==x==y=="+y);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
