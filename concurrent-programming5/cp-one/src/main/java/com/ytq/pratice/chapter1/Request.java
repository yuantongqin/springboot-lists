package com.ytq.pratice.chapter1;

import java.io.Serializable;

/**
 * @author yuantongqin
 * @date 2018/9/3 下午1:30
 */
public class Request implements Serializable{

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Request{" +
                "name='" + name + '\'' +
                '}';
    }
}
