package com.ytq.pratice;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Set;

/**
 * Hello world!
 */
public class App {

    int a = 10;

    protected  int b = 11;

    public static void main(String[] args) {


        ArrayList<String> list = new ArrayList<String>(Arrays.asList("a","b","c","d"));
        System.out.println(list.size());
        Iterator<String> iterator = list.iterator();
        while (iterator.hasNext()){
            String next = iterator.next();
//            if(next.equals("b") || next.equals("d")){
                iterator.remove();
//            }
        }
//        for(int i=0;i<size;i++){
//            list.remove(i);
//            System.out.println(list.size()+"--"+i);
//        }
        System.out.println(list);

    }




    public static boolean useList(String[] arr, String targetValue) {
        return Arrays.asList(arr).contains(targetValue);
    }

    public static boolean useSet(String[] arr, String targetValue) {
        Set<String> set = new HashSet<String>(Arrays.asList(arr));
        return set.contains(targetValue);
    }

    public static boolean useLoop(String[] arr, String targetValue) {
        for(String s: arr){
            if(s.equals(targetValue))
                return true;
        }
        return false;
    }

    public static boolean useArraysBinarySearch(String[] arr, String targetValue) {
        int a =  Arrays.binarySearch(arr, targetValue);
        if(a > 0)
            return true;
        else
            return false;
    }
}
