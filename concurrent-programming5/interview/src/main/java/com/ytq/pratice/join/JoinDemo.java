package com.ytq.pratice.join;

/**
 * @author yuantongqin
 * @date 2018/9/29 下午5:09
 */
public class JoinDemo {



    public static void main(String[] args) throws InterruptedException {
        final Thread t1 = new Thread(()->{
            System.out.println("t1");
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        Thread t2 = new Thread(()->{
            System.out.println("t2");
            try {
                t1.join();//主线程在执行他join方法
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        Thread t3 = new Thread(()->{
            System.out.println("t3");
        });
        t1.start();
//        t1.join();//主线程在执行他join方法
        System.out.println("不会执行");
        t2.start();
        t3.start();

    }
}
