package com.ytq.pratice.proxy.cglib;

import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

/**
 * @author yuantongqin
 * @date 2018/9/27 下午1:43
 */
public class CGLibProxy implements MethodInterceptor {

    private Object target;
    public Object  bind(Object target){
        this.target = target;
        Enhancer enhancer = new Enhancer();
        enhancer.setSuperclass(this.target.getClass());
        enhancer.setCallback(this);
        return enhancer.create();
    }

    @Override
    public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
        System.out.println("日志之前的处理");
        Object o1 = methodProxy.invokeSuper(o, objects);
        System.out.println("日志之后的处理");
        return o1;
    }
}
