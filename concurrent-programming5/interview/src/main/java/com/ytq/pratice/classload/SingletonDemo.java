package com.ytq.pratice.classload;

/**
 * @author yuantongqin
 * @date 2018/9/27 下午2:52
 */
public class SingletonDemo {

    private static SingletonDemo instance = new SingletonDemo();
    public static int a;
    public static int b = 0;

    private SingletonDemo(){
        a ++;
        b++;
    }
    public static SingletonDemo getInstance(){
        return instance;
    }

    public static void main(String[] args) {
        SingletonDemo.getInstance();
        System.out.println("a:=="+a);
        System.out.println("b:=="+b);
//        a = 1,b=0


    }


}
