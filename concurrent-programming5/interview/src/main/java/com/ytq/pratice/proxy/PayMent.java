package com.ytq.pratice.proxy;

/**
 * @author yuantongqin
 * @date 2018/9/26 下午4:40
 */
public interface PayMent {

    String doPlay(String uid);
}
