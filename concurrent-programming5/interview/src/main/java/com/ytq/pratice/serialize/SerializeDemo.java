package com.ytq.pratice.serialize;

import java.io.*;

/**
 * @author yuantongqin
 * @date 2018/9/27 下午5:32
 */
public class SerializeDemo {

    public static void main(String[] args) throws IOException, ClassNotFoundException {

        User user = new User();
        user.setName("张三");
        user.setSex("男");
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(baos);
        oos.writeObject(user);
        ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
        ObjectInputStream ois = new ObjectInputStream(bais);
        ois.readObject();


//        ReentrantLock

    }
}
