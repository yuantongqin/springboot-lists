package com.ytq.pratice.proxy.jdk;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * @author yuantongqin
 * @date 2018/9/26 下午5:15
 */
public class JdkDynamicProxy implements InvocationHandler {

    Object target;

    public Object bind(Object target){
        this.target = target;
        return Proxy.newProxyInstance(target.getClass().getClassLoader(),target.getClass().getInterfaces(),this);
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        System.out.println("日志处理之前");
        Object invoke = method.invoke(target, args);
        System.out.println("日志处理之后");
        return invoke;
    }
}
