package com.ytq.pratice.cloneable;

import java.io.Serializable;

/**
 * @author yuantongqin
 * @date 2018/9/27 下午3:40
 */
public class Email implements Serializable{

    private String content;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Email(String content) {
        this.content = content;
    }


}
