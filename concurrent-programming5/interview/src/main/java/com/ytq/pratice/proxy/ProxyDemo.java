package com.ytq.pratice.proxy;

import com.ytq.pratice.proxy.cglib.CGLibProxy;

/**
 * @author yuantongqin
 * @date 2018/9/26 下午4:38
 */
public class ProxyDemo {

    public static void main(String[] args) {
//      静态代理
//        PayMent payMent = new ThridChannelPay();
////        payMent.doPlay("change");
//        StaticThirdChannelPay pay = new StaticThirdChannelPay();
//        pay.bind(payMent);
//        pay.doPlay("change");

//        动态代理
//        PayMent payMent = new ThridChannelPay();
//        JdkDynamicProxy proxy = new JdkDynamicProxy();
//        PayMent bind = (PayMent)proxy.bind(payMent);
//        bind.doPlay("change");


//        byte[] file = ProxyGenerator.generateProxyClass("$Proxy0",ThridChannelPay.class.getInterfaces());
//        String path = "PaymentProxy.class";
//        try(FileOutputStream fileout = new FileOutputStream(path)){
//            fileout.write(file);
//            fileout.flush();
//            System.out.println("成功");
//            //这种方式不需要进行流的关闭
//        }catch (Exception e){
//            e.printStackTrace();
//        }

        ThridChannelPay payMent = new ThridChannelPay();
        CGLibProxy proxy = new CGLibProxy();
        ThridChannelPay pay = (ThridChannelPay) proxy.bind(payMent);
        System.out.println( pay.doPlay("change"));


    }
}
