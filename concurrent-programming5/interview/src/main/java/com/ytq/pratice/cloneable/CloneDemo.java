package com.ytq.pratice.cloneable;

import java.io.IOException;

/**
 * @author yuantongqin
 * @date 2018/9/27 下午4:40
 */
public class CloneDemo {

    public static void main(String[] args) throws CloneNotSupportedException, IOException, ClassNotFoundException {


        User user = new User();
        user.setName("张三");
        user.setEmail(new Email("进行看完10节课"));


        User user1 = (User) user.clone();
        user1.setName("李四");
        user1.getEmail().setContent("hhh");


        User user2 = (User) user.clone();
        user2.setName("什么");

        System.out.println(user.getEmail().getContent());
        System.out.println(user1.getEmail().getContent());
        System.out.println(user2.getEmail().getContent());


    }


}
