package com.ytq.pratice.proxy;

/**
 * @author yuantongqin
 * @date 2018/9/26 下午4:41
 */
public class ThridChannelPay implements PayMent {

    @Override
    public String doPlay(String uid) {
        System.out.println("通过第三方支付，支付成功"+uid);
        return "success";
    }
}
