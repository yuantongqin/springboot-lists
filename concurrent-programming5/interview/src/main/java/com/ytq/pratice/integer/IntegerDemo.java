package com.ytq.pratice.integer;

/**
 * @author yuantongqin
 * @date 2018/9/27 上午9:31
 */
public class IntegerDemo {

    private static User user = null;

    public static void main(String[] args) {

        user = new User("张三",18);
        System.out.println(user.toString());

        updateUser(user);
        System.out.println(user.toString());


    }

    private static User updateUser(User user) {
        user = new User("李四",20);
        user.setName("会话");
        user.setAge(22);
        return user;
    }


}
