package com.ytq.pratice.serialize;

import java.io.*;

/**
 * @author yuantongqin
 * @date 2018/9/27 下午3:40
 */
public class User implements Serializable{

    private String name;

    private transient String sex;

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", sex='" + sex + '\'' +
                '}';
    }
}
