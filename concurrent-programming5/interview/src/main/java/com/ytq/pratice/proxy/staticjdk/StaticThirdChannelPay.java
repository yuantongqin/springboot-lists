package com.ytq.pratice.proxy.staticjdk;

import com.ytq.pratice.proxy.PayMent;
import com.ytq.pratice.proxy.ThridChannelPay;

/**
 * @author yuantongqin
 * @date 2018/9/26 下午5:01
 */
public class StaticThirdChannelPay implements PayMent{


    PayMent payMent = new ThridChannelPay();
    public void bind(PayMent payMent){
        this.payMent = payMent;
    }

    @Override
    public String doPlay(String uid) {
        System.out.println("进行日志打印");
        return payMent.doPlay(uid);
    }
}
