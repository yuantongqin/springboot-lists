package com.ytq.pratice.threadlocal;

import java.util.Random;

/**
 * @author yuantongqin
 * @date 2018/9/29 下午5:32
 */
public class ThreadLocalDemo {

    private static int a = 0;
    private static int b = 0;
    static  ThreadLocal<Integer> num = new ThreadLocal<Integer>(){
        @Override
        protected Integer initialValue() {
            return 0;
        }
    };
    public static void main(String[] args) {
//        for (int i=0;i<5;i++){
//            new Thread(()->{
//
//                b = num.get().intValue()+5;
//                num.set(b);
//
//                a +=num.get().intValue();
//                System.out.println(a+"=="+num.get()+"==b=="+b);
//            },"thread"+i).start();
//        }
        ss();
    }

    static User user = new User("张三");
    private static String aa = "";
    private static String bb = "";
    static  ThreadLocal<User> hh = new ThreadLocal<User>(){
        @Override
        protected User initialValue() {
            return user ;
        }
    };
    public static void ss() {

        for (int i=0;i<5;i++){

            new Thread(()->{

                bb = hh.get().name;
                System.out.println(bb);
                 hh.get().name="李四"+new Random().nextInt(10);


            },"thread"+i).start();
        }
    }
}
