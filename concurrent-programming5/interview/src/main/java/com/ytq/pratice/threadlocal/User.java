package com.ytq.pratice.threadlocal;

/**
 * @author yuantongqin
 * @date 2018/10/10 下午4:36
 */
public class User {
    public String name;

    public User(String name) {
        this.name = name;
    }
}
