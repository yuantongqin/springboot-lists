package com.ytq.pratice;

/**
 * @author yuantongqin
 * @date 2018/9/9 下午5:08
 */
public class WaitDemo {

    Object lock;

    public WaitDemo(Object lock) {
        this.lock = lock;
    }

    public void  myWait(){
        synchronized (lock){
            try {
                System.out.println("wait 之前");
                lock.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("wait之后");
        }
        System.out.println("会话");
    }
}
