package com.ytq.pratice.executor;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author yuantongqin
 * @date 2018/9/13 下午2:44
 */
public class ExecutorDemo {

    public static void main(String[] args) {

        ExecutorService service =
        Executors.newFixedThreadPool(3);
//        Executors.newCachedThreadPool();
//        Executors.newSingleThreadExecutor();
//        Executors.newScheduledThreadPool()
        service.execute(new Runnable() {
            @Override
            public void run() {

            }
        });
    }
}
