package com.ytq.pratice;

/**
 * @author yuantongqin
 * @date 2018/9/9 下午5:10
 */
public class NotifyDemo {

    Object lock;

    public NotifyDemo(Object lock) {
        this.lock = lock;
    }

    public void myNotify(){
        synchronized (lock){//锁定的是同一个对象
            System.out.println("notify 之前");
            lock.notify();
            System.out.println("notify 之后");
        }
    }


}
