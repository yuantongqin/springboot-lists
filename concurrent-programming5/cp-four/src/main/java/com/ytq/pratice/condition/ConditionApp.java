package com.ytq.pratice.condition;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Hello world!
 */
public class ConditionApp {

    public static void main(String[] args) throws InterruptedException {

        Lock lock = new ReentrantLock();
        Condition condition = lock.newCondition();


        WaitDemo waitDemo = new WaitDemo(lock,condition);
        waitDemo.setName("wait线程111");
        waitDemo.start();

        NotifyDemo notifyDemo= new NotifyDemo(lock,condition);
        notifyDemo.setName("notify线程2222");
        notifyDemo.start();
    }
}
