package com.ytq.pratice.semaphore;

import java.util.concurrent.Semaphore;

/**
 * @author yuantongqin
 * @date 2018/9/13 上午11:28
 */
public class SemaphoreDemo {

    public static void main(String[] args) {
        /**
         * 设置permits许可证
         */
        Semaphore semaphore = new Semaphore(5);
        for (int i=0;i<10;i++){
            new myThread(semaphore,i).start();
        }
    }

    static class myThread extends Thread{
        Semaphore semaphore;
        int num;
        public myThread(Semaphore semaphore,int num) {
            this.semaphore = semaphore;
            this.num = num;
        }
        @Override
        public void run() {
            try {
                semaphore.acquire();
                System.out.println("获取锁"+num);
                Thread.sleep(2000);
                System.out.println("释放锁=="+num);
                semaphore.release();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    }

}
