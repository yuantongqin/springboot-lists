package com.ytq.pratice.cite;

/**
 * @author yuantongqin
 * @date 2018/9/18 下午7:04
 */
public class SoftDemo {

    public static void main(String[] args) throws InterruptedException {
        String user="张萨";
        String name = null;
        if(null!= user && !"".equals(user)&& user.length()>1){
            int length = user.length();
            StringBuffer sb = new StringBuffer();
            if(length >2){
                int len = length - 2;
                for (int i=0;i<len;i++){
                    sb.append("*");
                }
            }
            name = length>2?user.substring(0,1)+sb.toString()+user.substring(length-1,length):user.substring(0,1)+"*";

        }else{
            name =user;
        }
        System.out.println(name);

        String userPhone = "12345654542";
        if (userPhone!= null&& !"".equals(userPhone) && userPhone.length() > 7) {
           String  a= userPhone.substring(0, 3) + "****" + userPhone.substring(7);
            System.out.println(a);
        }
    }
}
