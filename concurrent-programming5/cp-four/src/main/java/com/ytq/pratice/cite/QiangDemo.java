package com.ytq.pratice.cite;

/**
 * @author yuantongqin
 * @date 2018/9/18 下午6:58
 */
public class QiangDemo {

    static Object o = new Object();
    public static void main(String[] args) {
        Object j = o;
        o = null;
        System.gc();
    }
}
