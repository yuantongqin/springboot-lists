package com.ytq.pratice.count;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Semaphore;

/**
 * @author yuantongqin
 * @date 2018/9/13 上午9:58
 */
public class CountDownTest {


    public static void main(String[] args) {

        final CountDownLatch downLatch = new CountDownLatch(2);


        try {
            new Thread(()->{

                try {
                    Thread.sleep(10000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                downLatch.countDown();
            }).start();
            new Thread(()->{

                try {
                    Thread.sleep(10001);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                downLatch.countDown();
            }).start();
            System.out.println("等待");

            downLatch.await();
            System.out.println("完成");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
