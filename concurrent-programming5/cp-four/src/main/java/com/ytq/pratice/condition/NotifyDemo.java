package com.ytq.pratice.condition;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;

/**
 * @author yuantongqin
 * @date 2018/9/9 下午5:10
 */
public class NotifyDemo extends Thread{

    Lock lock;
    Condition condition;

    public NotifyDemo(Lock lock,Condition condition) {
        this.lock = lock;
        this.condition = condition;
    }

    @Override
    public void run() {
        //锁定的是同一个对象
        try {
            lock.lock();
            System.out.println("notify 之前");
            condition.signal();
            System.out.println("notify 之后");
        } finally {
            lock.unlock();
        }
    }


}
