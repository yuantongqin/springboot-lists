package com.ytq.pratice.condition;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;

/**
 * @author yuantongqin
 * @date 2018/9/9 下午5:08
 */
public class WaitDemo extends Thread{

    Lock lock;
    Condition condition;

    public WaitDemo(Lock lock, Condition condition) {
        this.lock = lock;
        this.condition = condition;
    }

    @Override
    public void run() {
        try {
            lock.lock();
            System.out.println("wait 之前");
            condition.await();
            System.out.println("wait之后");
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }

    }

}
